import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Page {
    property var settings
    property alias volume: volumeSlider.value

    Frame {
        anchors.fill: parent
        Flickable {
            anchors.fill: parent
            //                contentWidth: settingsLayout.width
            contentHeight: settingsLayout.height

            ScrollBar.vertical: ScrollBar {}

            ColumnLayout {
                id: settingsLayout
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: 10

                GroupBox {
                    Layout.fillWidth: true
                    title: qsTr("Buttons")
                    GridLayout {
                        columns: 2
                        anchors.fill: parent

                        Label {
                            text: qsTr("Rows")
                        }
                        SpinBox {
                            id: rowSpinner
                            from: 1
                            to: 100
                            onValueChanged: settings.rows = value
                            value: settings.rows
                        }
                        Label {
                            text: qsTr("Columns")
                        }
                        SpinBox {
                            id: columnSpinner
                            from: 1
                            to: 100
                            onValueChanged: settings.columns = value
                            value: settings.columns
                        }
                    }
                }

                GroupBox {
                    Layout.fillWidth: true
                    title: qsTr("Volume")
                    Slider {
                        id: volumeSlider
                        anchors.fill: parent
                        value: 1.0
                    }
                }

                GroupBox {
                    Layout.fillWidth: true
                    title: qsTr("Usage")
                    GridLayout {
                        columns: 2
                        anchors.fill: parent

                        Label {
                            text: qsTr("Clicks while playing")
                        }
                        RowLayout {
                            Layout.fillWidth: true

                            RadioButton {
                                text: qsTr("Stop")
                                checked: !settings.restartOnClick
                                onCheckedChanged: settings.restartOnClick = !checked
                            }
                            RadioButton {
                                checked: settings.restartOnClick
                                text: qsTr("Restart")
                            }
                        }
                    }
                }

                Button {
                    text: qsTr("Reset sounds to defaults")
                    Layout.fillWidth: true
                    onClicked: {
                        root.sounds = referenceSounds
                        saveSounds()
                    }
                }
            }
        }
    }
}
