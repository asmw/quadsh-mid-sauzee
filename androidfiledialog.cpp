#include "androidfiledialog.h"
#include <QDebug>

AndroidFileDialog::ResultReceiver::ResultReceiver(AndroidFileDialog *dialog) : _dialog(dialog) {}
AndroidFileDialog::ResultReceiver::~ResultReceiver() {}

void AndroidFileDialog::ResultReceiver::handleActivityResult(int receiverRequestCode, int resultCode, const QAndroidJniObject &data)
{
    jint RESULT_OK = QAndroidJniObject::getStaticField<jint>("android/app/Activity", "RESULT_OK");
    if (receiverRequestCode == EXISTING_FILE_NAME_REQUEST && resultCode == RESULT_OK) {
        QAndroidJniObject uri = data.callObjectMethod("getData", "()Landroid/net/Uri;");
        QString path = QtAndroid::androidActivity().callObjectMethod("getPath","(Landroid/net/Uri;)Ljava/lang/String;", uri.object<jobject>()).toString();
        _dialog->emitExistingFileNameReady(path);
    } else {
        _dialog->emitExistingFileNameReady(QString());
    }
}

AndroidFileDialog::AndroidFileDialog(QObject *parent) : QObject(parent)
{
    receiver = new ResultReceiver(this);
}

AndroidFileDialog::~AndroidFileDialog()
{
    delete receiver;
}

bool AndroidFileDialog::provideExistingFileName()
{
    QAndroidJniObject ACTION_GET_CONTENT = QAndroidJniObject::fromString("android.intent.action.GET_CONTENT");
    QAndroidJniObject intent("android/content/Intent");
    if (ACTION_GET_CONTENT.isValid() && intent.isValid()) {
        intent.callObjectMethod("setAction", "(Ljava/lang/String;)Landroid/content/Intent;", ACTION_GET_CONTENT.object<jstring>());
        intent.callObjectMethod("setType", "(Ljava/lang/String;)Landroid/content/Intent;", QAndroidJniObject::fromString("audio/*").object<jstring>());
        QtAndroid::startActivity(intent.object<jobject>(), EXISTING_FILE_NAME_REQUEST, receiver);
        return true;
    } else {
        return false;
    }
}

void AndroidFileDialog::emitExistingFileNameReady(QString result)
{
    emit existingFileNameReady(result);
}
