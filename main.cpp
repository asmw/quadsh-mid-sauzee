#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

#ifdef Q_OS_ANDROID
#include "androidfiledialog.h"
#endif

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    app.setApplicationName("Quadsh-mid-sauzee");
    app.setOrganizationName("asmw.org");
    app.setApplicationVersion("1.1.0");

    QQmlApplicationEngine engine;

#ifdef Q_OS_ANDROID
    AndroidFileDialog fileDialog;
    engine.rootContext()->setContextProperty("fileDialog", &fileDialog);
    QQuickStyle::setStyle("Material");
#endif

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
