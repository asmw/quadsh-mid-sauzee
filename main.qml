import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.2

import QtMultimedia 5.6

import Qt.labs.settings 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    property var referenceSounds: {
        0: 'qrc:///sounds/badumtss.wav',
                1: 'qrc:///sounds/sad_trombone.wav',
                2: 'qrc:///sounds/burp1.wav',
                3: 'qrc:///sounds/burp2.wav',
                4: 'qrc:///sounds/fart1.wav',
                5: 'qrc:///sounds/fart2.wav',
                6: 'qrc:///sounds/crickets.wav',
                7: 'qrc:///sounds/whip.wav',
    }

    Settings {
        id: settings

        property int version: 110
        property alias volume: settingsPage.volume
        property string sounds: JSON.stringify(referenceSounds)
        property int rows: 4
        property int columns: 2
        property bool restartOnClick: false
    }

    signal updateSounds()

    property int currentIndex
    function selectSound(index) {
        currentIndex = index
        if(Qt.platform.os === "android") {
            fileDialog.provideExistingFileName()
        } else {
            fileSelector.open();
        }
    }

    Component {
        id: androidFileDialogComponent
        Connections {
            target: fileDialog
            onExistingFileNameReady: {
                root.sounds[currentIndex] = "file://" + encodeURIComponent(result);
                saveSounds();
            }
        }
    }

    Loader {
        enabled: Qt.platform.os === "android"
        sourceComponent: androidFileDialogComponent
    }

    property var sounds: ({})
    property bool initialized: false

    Component.onCompleted: {
        loadSounds()
        initialized = true
    }

    function loadSounds() {
        root.sounds = JSON.parse(settings.sounds);
        root.updateSounds()
    }

    onSoundsChanged: saveSounds()
    function saveSounds() {
        if(!initialized) return
        settings.sounds = JSON.stringify(root.sounds)
        root.updateSounds()
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page {
            id: buttonPage
            Frame {
                anchors.fill: parent
                GridLayout {
                    id: buttonLayout
                    columns: settings.columns
                    anchors.fill: parent
                    Repeater {
                        model: settings.rows * settings.columns

                        onModelChanged: root.updateSounds()

                        Button {
                            id: button
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            Layout.maximumWidth: buttonLayout.width/settings.columns

                            property string name: ""

                            text: name === "" ? "Sound " + index : name

                            Rectangle {
                                anchors.left: parent.left
                                anchors.top: parent.top
                                anchors.leftMargin: 5
                                anchors.topMargin: 10
                                width: button.height/10
                                height: width
                                radius: height
                                color: buttonSound.playbackState === Audio.PlayingState ? "green" : "transparent"
                            }

                            onClicked: {
                                if(buttonSound.hasAudio) {
                                    if(settings.restartOnClick) {
                                        buttonSound.stop();
                                        buttonSound.play();
                                    } else {
                                        if(buttonSound.playbackState === Audio.PlayingState) {
                                            buttonSound.stop();
                                        } else {
                                            buttonSound.play();
                                        }
                                    }
                                } else {
                                    root.selectSound(index)
                                }
                            }
                            onPressAndHold: root.selectSound(index)

                            Connections {
                                target: root
                                onUpdateSounds: buttonSound.load()
                            }

                            Audio {
                                id: buttonSound
                                audioRole: Audio.GameRole
                                volume: settings.volume

                                onSourceChanged: {
                                    if(!buttonSound.metaData.title) {
                                        button.name = source.toString().replace(/^.*[\\\/]/, '')
                                    } else {
                                        button.name = buttonSound.metaData.title
                                    }
                                }

                                function load() {
                                    source = root.sounds.hasOwnProperty(index) ? root.sounds[index] : ""
                                }
                            }

                            Rectangle {
                                id: playBar
                                height: 3
                                visible: buttonSound.playbackState === Audio.PlayingState
                                color: "black"
                                anchors.left: parent.left
                                anchors.bottom: parent.bottom
                                width: parent.width

                                onVisibleChanged: if(visible) {
                                                      playAnimation.restart();
                                                  } else {
                                                      playBar.width = 0
                                                  }

                                NumberAnimation {
                                    id: playAnimation
                                    duration: buttonSound.duration
                                    target: playBar
                                    property: "width"
                                    from: 0
                                    to: button.width
                                }
                            }
                        }
                    }
                }
            }

            FileDialog {
                id: fileSelector
                title: qsTr("Select a sound")
                onAccepted: {
                    console.log("accepted", fileUrl.toString())
                    root.sounds[currentIndex] = fileUrl.toString()
                    saveSounds()
                }
                nameFilters: ["Sounds (*.mp3 *.wav)"]
            }
        }

        SettingsPage {
            id: settingsPage
            settings: settings
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Sounds")
        }
        TabButton {
            text: qsTr("Settings")
        }
    }
}
