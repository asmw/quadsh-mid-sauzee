QT += qml quick quickcontrols2

CONFIG += c++11

SOURCES += main.cpp \
    quadshcore.cpp

android {
    QT -= widgets
    QT += androidextras

    SOURCES += androidfiledialog.cpp
    HEADERS += androidfiledialog.h
}

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/src/org/asmw/quadsh/*.java \
    ChangeLog

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    quadshcore.h
